fn simple_increment(increment: i32, target_height: i32) -> i32 {
    let mut count = 0;
    let mut guess = increment;

    while guess <= target_height {
        count += 1;
        guess += increment;
    }

    guess = guess - increment + 1;
    count += 1;

    while guess <= target_height {
        count += 1;
        guess += 1;
    }

    // println!("{}\t{}\t{}", count, guess - 1, target_height);
    return count;
}

fn multiply(amount: i32, target_height: i32) -> i32 {
    let mut count = 0;
    let mut guess = amount;

    while guess <= target_height {
        count += 1;
        guess *= amount;
    }

    guess = (guess / amount) + 1;
    count += 1;

    while guess <= target_height {
        count += 1;
        guess += 1;
    }

    // println!("{}\t{}\t{}", count, guess - 1, target_height);
    return count;
}

fn main() {
    let mut add_2 = 0;
    let mut add_3 = 0;
    let mut mult2 = 0;
    let mut mult3 = 0;

    for i in 1..101 {
        mult2 += multiply(2, i);
        mult3 += multiply(3, i);
        add_2 += simple_increment(2, i);
        add_3 += simple_increment(3, i);
        add_3 += simple_increment(3, i);
        add_3 += simple_increment(3, i);
        add_3 += simple_increment(3, i);
        add_3 += simple_increment(3, i);
        add_3 += simple_increment(3, i);
        add_3 += simple_increment(3, i);
        add_3 += simple_increment(3, i);
    }

    println!("Add 2:\t{}", add_2);
    println!("Add 3:\t{}", add_3);
    println!("Mult 2:\t{}", mult2);
    println!("Mult 3:\t{}", mult3);
}
