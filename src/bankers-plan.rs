fn fortune(f0: i32, p: f64, c0: i32, n: i32, i: f64) -> bool {
    let mut fnext = f0;
    let mut cnext = c0;

    for _ in 0..n {
        //println!("{:?}", fnext);
        fnext = fnext + (fnext as f64 * p / 100f64) as i32 - cnext;
        cnext = cnext + (cnext as f64 * i / 100f64) as i32;
        if fnext < 0 {
            return false;
        }
    }
    true
}

#[cfg(test)]
mod tests {
    use super::*;

    fn testequal(f0: i32, p: f64, c0: i32, n: i32, i: f64, exp: bool) -> () {
        assert_eq!(exp, fortune(f0, p, c0, n, i))
    }

    #[test]
    fn basics() {
        testequal(100000, 1.0, 2000, 15, 1.0, true);
        testequal(100000, 1.0, 9185, 12, 1.0, false);
        testequal(100000000, 1.0, 100000, 50, 1.0, true);
        testequal(100000000, 1.5, 10000000, 50, 1.0, false);
    }
}
