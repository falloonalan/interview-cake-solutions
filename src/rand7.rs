use rand::prelude::*;

// thread_rng is often the most convenient source of randomness:
let mut rng = thread_rng();
 
fn rand5() -> u32 {
    rng.gen_range(1, 6)
}

fn rand7() - u32 {
    rand5();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}