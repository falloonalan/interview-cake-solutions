# Interview Cake Solutions

My solutions, in Rust.

Files are a bit of a mess at the moment, maybe i'll fix em up if I do a few more poblems

## Find Rotation Point

https://www.interviewcake.com/question/python/find-rotation-point

I wrote my solution before seeing the their one, so it isn't as good as it could be
and I didn't bother with the bonus

## Reverse a linked list

https://www.interviewcake.com/question/python/reverse-linked-list

I am using https://rustbyexample.com/custom_types/enum/testcase_linked_list.html as a starting point

Okay so what I implimented was 'out of place', this was the bonus task from the challenge, but using
Rust it came to me as the natual solution. http://cglab.ca/~abeinges/blah/too-many-lists/book/first-push.html
Explains everything about linked lists in Rust, hopefully I'll get time to read it some day.
